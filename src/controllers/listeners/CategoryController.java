/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 */
package controllers.listeners;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDesktopPane;
import javax.swing.table.TableModel;
import models.tablemodel.CategoryTableModel;
import views.production.CategoryFrame;
import views.production.CategoryListFrame;

/**
 *
 * @author William Sanchez
 */
public class CategoryController implements ActionListener {
    CategoryFrame categoryFrame;
    CategoryListFrame categoryList;
    public CategoryController(CategoryFrame f){
        super();
        categoryFrame = f;
    }
     public CategoryController(CategoryListFrame l){
        super();
        categoryList = l;
    }   

         
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "save":

                break;
            case "clear":
                categoryFrame.clear();
                break;
            case "new":
                showCategoryForm(0);
                break;
            case "show":
                showCategoryForm(categoryList.getSelectedCategory());
                break;

        }        
    }
    
    public TableModel getCagories (){
        CategoryTableModel ctm = new CategoryTableModel();
        return ctm.getModel();
    }    

    private void showCategoryForm(int selectedCategory){
        categoryFrame = selectedCategory==0?new CategoryFrame():new CategoryFrame(selectedCategory);
        JDesktopPane desktop =  (JDesktopPane)categoryList.getParent();
        desktop.add(categoryFrame);
        categoryFrame.setVisible(true);        
    }     
     
}
