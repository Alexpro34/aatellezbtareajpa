/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 */
package controllers.listeners;

import controllers.Agenda.AgendaJpaController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.table.TableModel;
import models.Agenda.Agenda;
import models.tablemodel.AgendaTableModel;
import models.tablemodel.CategoryTableModel;
import views.Agenda.AgendaFrame;
import views.Agenda.AgendaListFrame;
import views.production.CategoryFrame;
import views.production.CategoryListFrame;

/**
 *
 * @author William Sanchez
 */
public class AgendaController implements ActionListener {

    AgendaFrame agendaFrame;
    AgendaListFrame agendaList;
    Agenda agenda;
    EntityManagerFactory emf;

    public AgendaController(AgendaFrame A) {
        super();
        agendaFrame = A;
        emf = Persistence.createEntityManagerFactory("AwJpaDemoPU");
    }

    public AgendaController(AgendaListFrame l) {
        super();
        agendaList = l;
        emf = Persistence.createEntityManagerFactory("AwJpaDemoPU");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "save":
                agenda = agendaFrame.getData();
                
                if(agenda.getId() == 0){
                    save(agenda);
                }else{
                    update(agenda);
                }
                break;
            case "clear":
                agendaFrame.clear();
                break;
            case "new":
                showCategoryForm(0);
                break;
            case "show":
                showCategoryForm(agendaList.getSelectedCategory());
                break;

        }
    }

    public TableModel getCagories() {
        AgendaTableModel ctm;
        AgendaJpaController db = new AgendaJpaController(emf);
        try {
            List<Agenda> agendas = db.findAgendaEntities();
            ctm = new AgendaTableModel(agendas);
        } catch (Exception e) {
            ctm = new AgendaTableModel();
            JOptionPane.showMessageDialog(agendaFrame, e.getMessage());
        }

        return ctm.getModel();
    }

    public Agenda getAgenda(int Id) {
        AgendaJpaController db = new AgendaJpaController(emf);
        Agenda agendas;

        try {
            agendas = db.findAgenda(Id);
        } catch (Exception e) {
            agendas = new Agenda();
        }

        return agendas;
    }

    public void save(Agenda agendas) {
        AgendaJpaController db = new AgendaJpaController(emf);
        try {
            db.create(agenda);
            refreshList();
            JOptionPane.showMessageDialog(agendaFrame, "Ingresado Correctamente");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(agendaFrame, e.getMessage());
        }
    }

    public void update(Agenda agendas) {
        AgendaJpaController db = new AgendaJpaController(emf);
        try {
            db.edit(agenda);
            refreshList();
            JOptionPane.showMessageDialog(agendaFrame, "Actualizado Correctamente");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(agendaFrame, e.getMessage());
        }
    }

   private void showCategoryForm(int selectedCategory) {
        agendaFrame = selectedCategory == 0 ? new AgendaFrame() : new AgendaFrame(selectedCategory);
        JDesktopPane desktop = (JDesktopPane) agendaList.getParent();
        desktop.add(agendaFrame);
        agendaFrame.setVisible(true);
    }

   public void refreshList(){
       JDesktopPane desktop = (JDesktopPane)agendaFrame.getDesktopPane();
       for(JInternalFrame frame: desktop.getAllFrames()){
           if(AgendaListFrame.class.getName()==frame.getClass().getName()){
               ((AgendaListFrame)frame).refreshList();
           }
       }
   }
}
